﻿using System;

namespace db_hw
{
    public static class Program
    {
        public static void Main(string[] args)
        {
            bool infinity = true;
            DBworker Dbw = new();

            while (infinity)
            {
                Console.WriteLine("Введите цифру для действия и нажмите Enter: \n 1 - Вывод всех даннных \n 2 - Ввод нового продавца\n 3 - Ввод новой категории\n 4 - Ввод нового товара \n 5 - выход");
                try
                {
                    var numb = Console.ReadLine();
                    Console.WriteLine("\n");
                    switch (numb)
                    {
                        case "1": Dbw.OutAllTables(); break;
                        case "2": Dbw.AddOwners(); break;
                        case "3": Dbw.AddCategory(); break;
                        case "4": Dbw.AddProduct(); break;
                        case "5": infinity = false; break;
                        default: Console.WriteLine("Что - то введено не то. Перепроверьте"); break;
                    }
                    Console.WriteLine("\n");
                }
                catch
                {
                    Console.WriteLine("Что - то введено не то. Перепроверьте");
                }
            }
        }
    }
}
