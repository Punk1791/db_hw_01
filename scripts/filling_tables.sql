INSERT INTO owners VALUES (1, 'Vanya', 'Ivan', 'men', 'telegram:@Vanya', TIMESTAMP '2004-10-19');
INSERT INTO owners VALUES (2, 'Maria', 'DB', 'women', 'telegram:@MrsMariaDB', TIMESTAMP '1996-10-19');
INSERT INTO owners VALUES (3, 'Anton', 'Redis', 'men', 'otus:@Anton', TIMESTAMP '1986-01-01');
INSERT INTO owners VALUES (4, 'Stanislav', 'Shupurin', 'men', 'psql:@postgres', TIMESTAMP '1986-01-12');
INSERT INTO owners VALUES (5, 'Polina', 'Makarova', 'women', 'WhatsApp:@+79638527496', TIMESTAMP '2000-01-12');

INSERT INTO category VALUES (1, 'Avto');
INSERT INTO category VALUES (2, 'Databases');
INSERT INTO category VALUES (3, 'Services');
INSERT INTO category VALUES (4, 'Property');
INSERT INTO category VALUES (5, 'Other');

INSERT INTO product VALUES (1, 1, 'Cossack', 'I sell a Cossack in good condition. Bargain', '10000', 1, TIMESTAMP '2020-10-19');
INSERT INTO product VALUES (2, 2, 'Data', 'Sell merged database', '70000', 2, TIMESTAMP '2022-01-02');
INSERT INTO product VALUES (3, 3, 'Data from redis', 'Sell merged student database', '500', 2, TIMESTAMP '2022-01-04');
INSERT INTO product VALUES (4, 4, 'Administrative Services', 'Database Administration Services', '1000', 3, TIMESTAMP '2021-04-04');
INSERT INTO product VALUES (5, 5, 'Internet store', 'I sell a ready-made site based on the project of the Cossacks store', '90000', 5, TIMESTAMP '2022-05-04');