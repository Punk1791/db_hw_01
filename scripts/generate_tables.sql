CREATE TABLE Owners (    
	id int NOT NULL PRIMARY KEY,                
	first_name VARCHAR(50),
	last_name VARCHAR(50), 
	gender VARCHAR(5),
    contact VARCHAR(50),
	date_of_birth DATE   
	);

CREATE TABLE Category (    
	id int NOT NULL PRIMARY KEY,
	category_name VARCHAR(50)
	);

CREATE TABLE Product (    
	id int NOT NULL PRIMARY KEY,
    owner_id int REFERENCES owners(id),               
	product_name VARCHAR(50),
	description_name VARCHAR(350), 
	price VARCHAR(5),     
	category int REFERENCES category (id),
	date_of_appearance DATE,
    FOREIGN KEY (owner_id) REFERENCES owners(id)
	);