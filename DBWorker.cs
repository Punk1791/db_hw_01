using System;
using Microsoft.EntityFrameworkCore;

namespace db_hw
{

    /// <summary>
    ///  Служебный класс для работы с контекстом и соединением к БД
    /// </summary>
    internal class ApplicationContext : DbContext
    {
        public DbSet<owners> owners { get; set; }
        public DbSet<category> category { get; set; }
        public DbSet<product> product { get; set; }
        public ApplicationContext()
        {
            Database.EnsureCreated();
        }
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseNpgsql("Host=localhost;Port=5432;Database=avito;Username=postgres;Password=postgres");
        }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<owners>()
                        .Property(f => f.id)
                        .ValueGeneratedOnAdd();

            modelBuilder.Entity<category>()
                        .Property(f => f.id)
                        .ValueGeneratedOnAdd();

            modelBuilder.Entity<product>()
                        .Property(f => f.id)
                        .ValueGeneratedOnAdd();
        }
    }

    /// <summary>
    ///  Класс для внешнего взаимодействия с БД
    /// </summary>
    public class DBworker
    {

        /// <summary>
        ///  Функция вывода даннных
        /// </summary>
        public void OutAllTables()
        {
            using (var context = new ApplicationContext())
            {
                Console.WriteLine("___Owners___");
                foreach (var i in context.owners)
                {
                    Console.WriteLine($"{i.id} {i.first_name} {i.last_name} {i.gender} {i.contact} {i.date_of_birth}");
                }

                Console.WriteLine("\n___Categories___");
                foreach (var i in context.category)
                {
                    Console.WriteLine($"{i.id} {i.category_name}");
                }

                Console.WriteLine("\n___Products___");
                foreach (var i in context.product)
                {
                    Console.WriteLine($"{i.id} {i.owner_id} {i.product_name} {i.description_name} {i.price} {i.category} {i.date_of_appearance}");
                }
            }
        }

        /// <summary>
        ///  Функция добавления пользователей
        /// </summary>
        public void AddOwners()
        {
            try
            {
                var Owner = new owners();
                Console.WriteLine("Добавление пользователя. Введите:");

                Console.WriteLine("Имя:");
                Owner.first_name = Console.ReadLine();

                Console.WriteLine("Фамилия:");
                Owner.last_name = Console.ReadLine();

                Console.WriteLine("Пол:");
                Owner.gender = Console.ReadLine();

                Console.WriteLine("Контактные данные:");
                Owner.contact = Console.ReadLine();

                Console.WriteLine("Дата рождения:");
                Owner.date_of_birth = Convert.ToDateTime(Console.ReadLine());

                using (var context = new ApplicationContext())
                {
                    context.owners.Add(Owner);
                    context.SaveChanges();
                }
                Console.WriteLine("Добавлено!");
            }
            catch(Exception e)
            {
                Console.WriteLine("Что - то пошло не так! "+ e.Message);
            }
        }

        /// <summary>
        ///  Функция добавления категории
        /// </summary>
        public void AddCategory()
        {
            try
            {
                var Category = new category();
                Console.WriteLine("Добавление категории. Введите:");

                Console.WriteLine("Имя:");
                Category.category_name = Console.ReadLine();

                using (var context = new ApplicationContext())
                {
                    context.category.Add(Category);
                    context.SaveChanges();
                }
                Console.WriteLine("Добавлено!");
            }
            catch(Exception e)
            {
                Console.WriteLine("Что - то пошло не так! "+ e.Message);
            }
        }

        /// <summary>
        ///  Функция добавления продукта
        /// </summary>
        public void AddProduct()
        {
            try
            {
                var Product = new product();
                Console.WriteLine("Добавление продукта. Введите:");

                Console.WriteLine("Идентификатор пользователя:");
                Product.owner_id = Convert.ToInt32(Console.ReadLine());

                Console.WriteLine("Название продукта:");
                Product.product_name = Console.ReadLine();

                Console.WriteLine("Подробности продукта:");
                Product.description_name = Console.ReadLine();

                Console.WriteLine("Цена:");
                Product.price = Console.ReadLine();

                Console.WriteLine("Идентификатор категории:");
                Product.category = Convert.ToInt32(Console.ReadLine());

                Product.date_of_appearance = DateTime.Now;

                using (var context = new ApplicationContext())
                {
                    context.product.Add(Product);
                    context.SaveChanges();
                }
                Console.WriteLine("Добавлено!");
            }
            catch(Exception e)
            {
                Console.WriteLine("Что - то пошло не так! "+ e.Message);
            }
        }
    }
}