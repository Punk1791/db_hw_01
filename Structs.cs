using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace db_hw
{
    public class owners
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int id {get; set;}
        public string first_name {get; set;}
        public string last_name {get; set;}
        public string gender {get; set;}
        public string contact {get; set;}
        public DateTime date_of_birth {get; set;}
    }

    public class category
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int id {get; set;}
        public string category_name {get; set;}
    }

    public class product
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int id {get; set;}
        public int owner_id {get; set;}
        public string product_name {get; set;}
        public string description_name {get; set;}
        public string price {get; set;}
        public int category {get; set;}
        public DateTime date_of_appearance {get; set;}
    }
}